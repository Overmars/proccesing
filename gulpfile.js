var gulp         = require('gulp');
var connect      = require('gulp-connect');
var stylus       = require('gulp-stylus');
var sourcemaps   = require('gulp-sourcemaps');
var autoprefixer = require('gulp-autoprefixer');
var plumber      = require('gulp-plumber');
var rigger       = require('gulp-rigger');

// Local server
gulp.task('connect', function () {
  connect.server({
    root: ['dev'],
    livereload: true
  });
});

//HTML
gulp.task('html', function() {
  return gulp.src("dev/*.html")
    .pipe(connect.reload());
});

//Images
gulp.task('images', function() {
  return gulp.src("dev/images/*.*")
    .pipe(connect.reload());
});

//JS
gulp.task('js', function() {
  return gulp.src("dev/js/*.js")
    .pipe(connect.reload());
});

// Stylus
gulp.task('stylus', function() {
  return gulp.src("dev/stylus/styles.styl")
    .pipe(plumber())
    .pipe(sourcemaps.init())
    .pipe(stylus({compress: false}))
    .pipe(autoprefixer())
    .pipe(sourcemaps.write())
    .pipe(gulp.dest("dev/css"))
    .pipe(connect.reload());
});

gulp.task('watch', function () {
  gulp.watch("dev/*.html", ['html']);
  gulp.watch("dev/images/*.*", ['images']);
  gulp.watch("dev/js/*.js", ['js']);
  gulp.watch("dev/stylus/styles.styl", ['stylus']);
  gulp.watch("dev/stylus/libs/*.styl", ['stylus']);
  gulp.watch("dev/stylus/projects/*.styl", ['stylus']);
});

// Watching project files
gulp.task('default', ['watch', 'html', 'images', 'stylus', 'js', 'connect']);